package com.santander.api.factory.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.santander.api.factory.model.DataObjectDigitalCustomersRequestDomainModelAPI;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Información requerida para crear un cliente en Santander.
 */
@ApiModel(description = "Información requerida para crear un cliente en Santander.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-09T11:25:09.241-05:00")

public class DigitalCustomersRequestDomainModelAPI  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("data")
  private DataObjectDigitalCustomersRequestDomainModelAPI data = null;

  public DigitalCustomersRequestDomainModelAPI data(DataObjectDigitalCustomersRequestDomainModelAPI data) {
    this.data = data;
    return this;
  }

  /**
   * Información requrida como JSON de entrada.
   * @return data
  **/
  @ApiModelProperty(required = true, value = "Información requrida como JSON de entrada.")
  @NotNull

  @Valid

  public DataObjectDigitalCustomersRequestDomainModelAPI getData() {
    return data;
  }

  public void setData(DataObjectDigitalCustomersRequestDomainModelAPI data) {
    this.data = data;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DigitalCustomersRequestDomainModelAPI digitalCustomersRequest = (DigitalCustomersRequestDomainModelAPI) o;
    return Objects.equals(this.data, digitalCustomersRequest.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DigitalCustomersRequestDomainModelAPI {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

