package com.santander.api.factory.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Definición del objeto clientContactPhoneDetail.
 */
@ApiModel(description = "Definición del objeto clientContactPhoneDetail.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-09T11:25:09.241-05:00")

public class ClientContactPhoneDetailDomainModelAPI  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("phoneNumber")
  private String phoneNumber = null;

  public ClientContactPhoneDetailDomainModelAPI phoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
    return this;
  }

  /**
   * Número del teléfono.
   * @return phoneNumber
  **/
  @ApiModelProperty(example = "442 795 1234", required = true, value = "Número del teléfono.")
  @NotNull


  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClientContactPhoneDetailDomainModelAPI clientContactPhoneDetail = (ClientContactPhoneDetailDomainModelAPI) o;
    return Objects.equals(this.phoneNumber, clientContactPhoneDetail.phoneNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(phoneNumber);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClientContactPhoneDetailDomainModelAPI {\n");
    
    sb.append("    phoneNumber: ").append(toIndentedString(phoneNumber)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

