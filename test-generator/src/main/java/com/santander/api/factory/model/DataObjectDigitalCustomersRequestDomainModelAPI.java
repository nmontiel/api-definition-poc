package com.santander.api.factory.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.santander.api.factory.model.ClientDataDetailDomainModelAPI;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Definición del objeto data para el objeto digitalCustomersRq.
 */
@ApiModel(description = "Definición del objeto data para el objeto digitalCustomersRq.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-09T11:25:09.241-05:00")

public class DataObjectDigitalCustomersRequestDomainModelAPI  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("clientDataDetail")
  private ClientDataDetailDomainModelAPI clientDataDetail = null;

  public DataObjectDigitalCustomersRequestDomainModelAPI clientDataDetail(ClientDataDetailDomainModelAPI clientDataDetail) {
    this.clientDataDetail = clientDataDetail;
    return this;
  }

  /**
   * Detalle de los datos de un cliente.
   * @return clientDataDetail
  **/
  @ApiModelProperty(required = true, value = "Detalle de los datos de un cliente.")
  @NotNull

  @Valid

  public ClientDataDetailDomainModelAPI getClientDataDetail() {
    return clientDataDetail;
  }

  public void setClientDataDetail(ClientDataDetailDomainModelAPI clientDataDetail) {
    this.clientDataDetail = clientDataDetail;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataObjectDigitalCustomersRequestDomainModelAPI dataObjectDigitalCustomersRequest = (DataObjectDigitalCustomersRequestDomainModelAPI) o;
    return Objects.equals(this.clientDataDetail, dataObjectDigitalCustomersRequest.clientDataDetail);
  }

  @Override
  public int hashCode() {
    return Objects.hash(clientDataDetail);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataObjectDigitalCustomersRequestDomainModelAPI {\n");
    
    sb.append("    clientDataDetail: ").append(toIndentedString(clientDataDetail)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

