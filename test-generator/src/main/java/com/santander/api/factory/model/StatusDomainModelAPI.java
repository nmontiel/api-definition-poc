package com.santander.api.factory.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Definición del objeto status
 */
@ApiModel(description = "Definición del objeto status")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-09T11:25:09.241-05:00")

public class StatusDomainModelAPI  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("serverStatusCode")
  private String serverStatusCode = null;

  @JsonProperty("severity")
  private String severity = null;

  @JsonProperty("statusCode")
  private String statusCode = null;

  @JsonProperty("statusDesc")
  private String statusDesc = null;

  public StatusDomainModelAPI serverStatusCode(String serverStatusCode) {
    this.serverStatusCode = serverStatusCode;
    return this;
  }

  /**
   * Server Status Code. The value placed here is used to allow the client to display the status code to the user. This allows the user to read the code to a customer service representative for debugging purposes.
   * @return serverStatusCode
  **/
  @ApiModelProperty(example = "200", value = "Server Status Code. The value placed here is used to allow the client to display the status code to the user. This allows the user to read the code to a customer service representative for debugging purposes.")


  public String getServerStatusCode() {
    return serverStatusCode;
  }

  public void setServerStatusCode(String serverStatusCode) {
    this.serverStatusCode = serverStatusCode;
  }

  public StatusDomainModelAPI severity(String severity) {
    this.severity = severity;
    return this;
  }

  /**
   * Severity. Valid values: Error, Warn, Info
   * @return severity
  **/
  @ApiModelProperty(example = "INFO", value = "Severity. Valid values: Error, Warn, Info")


  public String getSeverity() {
    return severity;
  }

  public void setSeverity(String severity) {
    this.severity = severity;
  }

  public StatusDomainModelAPI statusCode(String statusCode) {
    this.statusCode = statusCode;
    return this;
  }

  /**
   * Response Status Code. Valid values depend on context. See Response Code List for a complete list of Response Status Codes.
   * @return statusCode
  **/
  @ApiModelProperty(example = "SCF000", value = "Response Status Code. Valid values depend on context. See Response Code List for a complete list of Response Status Codes.")


  public String getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(String statusCode) {
    this.statusCode = statusCode;
  }

  public StatusDomainModelAPI statusDesc(String statusDesc) {
    this.statusDesc = statusDesc;
    return this;
  }

  /**
   * Status Description. Explanatory text associated with the status code. The Status Description may be default text or Service Provider specific.
   * @return statusDesc
  **/
  @ApiModelProperty(example = "Operación Exitosa", value = "Status Description. Explanatory text associated with the status code. The Status Description may be default text or Service Provider specific.")


  public String getStatusDesc() {
    return statusDesc;
  }

  public void setStatusDesc(String statusDesc) {
    this.statusDesc = statusDesc;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StatusDomainModelAPI status = (StatusDomainModelAPI) o;
    return Objects.equals(this.serverStatusCode, status.serverStatusCode) &&
        Objects.equals(this.severity, status.severity) &&
        Objects.equals(this.statusCode, status.statusCode) &&
        Objects.equals(this.statusDesc, status.statusDesc);
  }

  @Override
  public int hashCode() {
    return Objects.hash(serverStatusCode, severity, statusCode, statusDesc);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StatusDomainModelAPI {\n");
    
    sb.append("    serverStatusCode: ").append(toIndentedString(serverStatusCode)).append("\n");
    sb.append("    severity: ").append(toIndentedString(severity)).append("\n");
    sb.append("    statusCode: ").append(toIndentedString(statusCode)).append("\n");
    sb.append("    statusDesc: ").append(toIndentedString(statusDesc)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

