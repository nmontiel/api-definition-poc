package com.santander.api.factory.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Objeto con la descripción del error.
 */
@ApiModel(description = "Objeto con la descripción del error.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-09T11:25:09.241-05:00")

public class ErrorsDomainModelAPI  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("code")
  private String code = null;

  @JsonProperty("message")
  private String message = null;

  @JsonProperty("level")
  private String level = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("moreInfo")
  private String moreInfo = null;

  public ErrorsDomainModelAPI code(String code) {
    this.code = code;
    return this;
  }

  /**
   * Código de error
   * @return code
  **/
  @ApiModelProperty(example = "No data found", value = "Código de error")


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public ErrorsDomainModelAPI message(String message) {
    this.message = message;
    return this;
  }

  /**
   * Mensaje de error
   * @return message
  **/
  @ApiModelProperty(example = "No data", value = "Mensaje de error")


  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public ErrorsDomainModelAPI level(String level) {
    this.level = level;
    return this;
  }

  /**
   * Nivel de error
   * @return level
  **/
  @ApiModelProperty(example = "INFO", value = "Nivel de error")


  public String getLevel() {
    return level;
  }

  public void setLevel(String level) {
    this.level = level;
  }

  public ErrorsDomainModelAPI description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Descripción del error
   * @return description
  **/
  @ApiModelProperty(example = "Happens when the customer does not exist in database", value = "Descripción del error")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ErrorsDomainModelAPI moreInfo(String moreInfo) {
    this.moreInfo = moreInfo;
    return this;
  }

  /**
   * Más información
   * @return moreInfo
  **/
  @ApiModelProperty(example = "There is no customer with the data provided", value = "Más información")


  public String getMoreInfo() {
    return moreInfo;
  }

  public void setMoreInfo(String moreInfo) {
    this.moreInfo = moreInfo;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ErrorsDomainModelAPI errors = (ErrorsDomainModelAPI) o;
    return Objects.equals(this.code, errors.code) &&
        Objects.equals(this.message, errors.message) &&
        Objects.equals(this.level, errors.level) &&
        Objects.equals(this.description, errors.description) &&
        Objects.equals(this.moreInfo, errors.moreInfo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, message, level, description, moreInfo);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ErrorsDomainModelAPI {\n");
    
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    level: ").append(toIndentedString(level)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    moreInfo: ").append(toIndentedString(moreInfo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

