package com.santander.api.factory.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.santander.api.factory.model.ClientContactPhoneDetailDomainModelAPI;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Definición del objeto clientDataDetail.
 */
@ApiModel(description = "Definición del objeto clientDataDetail.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-09T11:25:09.241-05:00")

public class ClientDataDetailDomainModelAPI  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("paternalName")
  private String paternalName = null;

  @JsonProperty("maternalName")
  private String maternalName = null;

  @JsonProperty("gender")
  private String gender = null;

  @JsonProperty("birthDate")
  private String birthDate = null;

  @JsonProperty("birthPlace")
  private String birthPlace = null;

  @JsonProperty("branchOffice")
  private String branchOffice = null;

  @JsonProperty("clientContactPhoneDetail")
  private ClientContactPhoneDetailDomainModelAPI clientContactPhoneDetail = null;

  public ClientDataDetailDomainModelAPI name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Nombre del cliente.
   * @return name
  **/
  @ApiModelProperty(example = "Jaime", required = true, value = "Nombre del cliente.")
  @NotNull


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ClientDataDetailDomainModelAPI paternalName(String paternalName) {
    this.paternalName = paternalName;
    return this;
  }

  /**
   * Apellido paterno.
   * @return paternalName
  **/
  @ApiModelProperty(example = "Pérez", required = true, value = "Apellido paterno.")
  @NotNull


  public String getPaternalName() {
    return paternalName;
  }

  public void setPaternalName(String paternalName) {
    this.paternalName = paternalName;
  }

  public ClientDataDetailDomainModelAPI maternalName(String maternalName) {
    this.maternalName = maternalName;
    return this;
  }

  /**
   * Apellido materno.
   * @return maternalName
  **/
  @ApiModelProperty(example = "González", required = true, value = "Apellido materno.")
  @NotNull


  public String getMaternalName() {
    return maternalName;
  }

  public void setMaternalName(String maternalName) {
    this.maternalName = maternalName;
  }

  public ClientDataDetailDomainModelAPI gender(String gender) {
    this.gender = gender;
    return this;
  }

  /**
   * Género (Masculino/femenino).
   * @return gender
  **/
  @ApiModelProperty(example = "Masculino", required = true, value = "Género (Masculino/femenino).")
  @NotNull


  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public ClientDataDetailDomainModelAPI birthDate(String birthDate) {
    this.birthDate = birthDate;
    return this;
  }

  /**
   * Fecha de nacimiento del cliente.
   * @return birthDate
  **/
  @ApiModelProperty(example = "1996-04-28", required = true, value = "Fecha de nacimiento del cliente.")
  @NotNull


  public String getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(String birthDate) {
    this.birthDate = birthDate;
  }

  public ClientDataDetailDomainModelAPI birthPlace(String birthPlace) {
    this.birthPlace = birthPlace;
    return this;
  }

  /**
   * Revisar si es citinzenship.
   * @return birthPlace
  **/
  @ApiModelProperty(example = "Madrid", required = true, value = "Revisar si es citinzenship.")
  @NotNull


  public String getBirthPlace() {
    return birthPlace;
  }

  public void setBirthPlace(String birthPlace) {
    this.birthPlace = birthPlace;
  }

  public ClientDataDetailDomainModelAPI branchOffice(String branchOffice) {
    this.branchOffice = branchOffice;
    return this;
  }

  /**
   * Sucursal asignada al cliente.
   * @return branchOffice
  **/
  @ApiModelProperty(example = "78.0", required = true, value = "Sucursal asignada al cliente.")
  @NotNull


  public String getBranchOffice() {
    return branchOffice;
  }

  public void setBranchOffice(String branchOffice) {
    this.branchOffice = branchOffice;
  }

  public ClientDataDetailDomainModelAPI clientContactPhoneDetail(ClientContactPhoneDetailDomainModelAPI clientContactPhoneDetail) {
    this.clientContactPhoneDetail = clientContactPhoneDetail;
    return this;
  }

  /**
   * Datos de contacto telefónico.
   * @return clientContactPhoneDetail
  **/
  @ApiModelProperty(required = true, value = "Datos de contacto telefónico.")
  @NotNull

  @Valid

  public ClientContactPhoneDetailDomainModelAPI getClientContactPhoneDetail() {
    return clientContactPhoneDetail;
  }

  public void setClientContactPhoneDetail(ClientContactPhoneDetailDomainModelAPI clientContactPhoneDetail) {
    this.clientContactPhoneDetail = clientContactPhoneDetail;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClientDataDetailDomainModelAPI clientDataDetail = (ClientDataDetailDomainModelAPI) o;
    return Objects.equals(this.name, clientDataDetail.name) &&
        Objects.equals(this.paternalName, clientDataDetail.paternalName) &&
        Objects.equals(this.maternalName, clientDataDetail.maternalName) &&
        Objects.equals(this.gender, clientDataDetail.gender) &&
        Objects.equals(this.birthDate, clientDataDetail.birthDate) &&
        Objects.equals(this.birthPlace, clientDataDetail.birthPlace) &&
        Objects.equals(this.branchOffice, clientDataDetail.branchOffice) &&
        Objects.equals(this.clientContactPhoneDetail, clientDataDetail.clientContactPhoneDetail);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, paternalName, maternalName, gender, birthDate, birthPlace, branchOffice, clientContactPhoneDetail);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClientDataDetailDomainModelAPI {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    paternalName: ").append(toIndentedString(paternalName)).append("\n");
    sb.append("    maternalName: ").append(toIndentedString(maternalName)).append("\n");
    sb.append("    gender: ").append(toIndentedString(gender)).append("\n");
    sb.append("    birthDate: ").append(toIndentedString(birthDate)).append("\n");
    sb.append("    birthPlace: ").append(toIndentedString(birthPlace)).append("\n");
    sb.append("    branchOffice: ").append(toIndentedString(branchOffice)).append("\n");
    sb.append("    clientContactPhoneDetail: ").append(toIndentedString(clientContactPhoneDetail)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

