package com.santander.api.factory.test.model;

import org.junit.Test;

import com.santander.api.factory.model.ClientContactPhoneDetailDomainModelAPI;
import com.santander.api.factory.model.ClientDataDetailDomainModelAPI;
import com.santander.api.factory.model.DataObjectDigitalCustomersRequestDomainModelAPI;
import com.santander.api.factory.model.DigitalCustomersRequestDomainModelAPI;

/**
 * Prueba unitaria que genera los json de pruebas
 * 
 * @author nmontiel
 *
 */
public class RequestGenerator {

	@Test
	public void generateRequest() {

		DigitalCustomersRequestDomainModelAPI request = new DigitalCustomersRequestDomainModelAPI();
		DataObjectDigitalCustomersRequestDomainModelAPI data = new DataObjectDigitalCustomersRequestDomainModelAPI();
		ClientDataDetailDomainModelAPI clientDataDetail = new ClientDataDetailDomainModelAPI();
		clientDataDetail.setBirthDate("birthDate");
		clientDataDetail.setBirthPlace("birthPlace");
		clientDataDetail.setBranchOffice("branchOffice");
		ClientContactPhoneDetailDomainModelAPI clientContactPhoneDetail = new ClientContactPhoneDetailDomainModelAPI();
		clientContactPhoneDetail.setPhoneNumber("23456789");
		clientDataDetail.setClientContactPhoneDetail(clientContactPhoneDetail);
		clientDataDetail.setGender("gender");
		clientDataDetail.setMaternalName("maternalName");
		clientDataDetail.setName("name");
		clientDataDetail.setPaternalName("paternalName");

		data.setClientDataDetail(clientDataDetail);
		request.setData(data);

		
	
		
		
	}

}
