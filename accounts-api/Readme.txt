#GEneracion de artefactos via cmd
java -jar /opt/software/swagger/swagger-code-gen/swagger-codegen-cli-2.2.3.jar generate -i digital-customer.yaml  -l java

# Diff con kdiff
git difftool -y


# Generar archivos y codigo por medio del pom
mvn -f services-generator.xml clean generate-sources -Dmaven.test.skip=true -U